//
//  ViewController.m
//  Labb_1
//
//  Created by Stjernström on 2015-01-19.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *colorDisplay;
@property (weak, nonatomic) IBOutlet UISlider *colorRed;
@property (weak, nonatomic) IBOutlet UISlider *colorGreen;
@property (weak, nonatomic) IBOutlet UISlider *colorBlue;

@end

@implementation ViewController

- (UIColor*) currentColor {
    return [UIColor colorWithRed:self.colorRed.value green:self.colorGreen.value blue:self.colorBlue.value alpha:1.0f];
}

- (IBAction)redColorChange:(id)sender {
    self.colorDisplay.backgroundColor = [self currentColor];
}

- (IBAction)greenColorChange:(id)sender {
    self.colorDisplay.backgroundColor = [self currentColor];
}
- (IBAction)blueColorChange:(id)sender {
    self.colorDisplay.backgroundColor = [self currentColor];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
